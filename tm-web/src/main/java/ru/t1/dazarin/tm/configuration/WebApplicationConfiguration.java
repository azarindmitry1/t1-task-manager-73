package ru.t1.dazarin.tm.configuration;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.t1.dazarin.tm.endpoint.AuthEndpointImpl;
import ru.t1.dazarin.tm.endpoint.ProjectEndpointImpl;
import ru.t1.dazarin.tm.endpoint.TaskEndpointImpl;

import javax.xml.ws.Endpoint;


@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Bus bus;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/").permitAll()
//                .antMatchers("/ws/*").permitAll()
//                .antMatchers("/api/auth/login").permitAll()
//
//                .and()
//                .exceptionHandling()
//                .authenticationEntryPoint(new AuthenticationEntryPoint())
//                .and()
//                .authorizeRequests()
//
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .loginProcessingUrl("/auth")
//                .and()
//                .logout().permitAll()
//                .logoutSuccessUrl("/login")
//                .and()
//                .csrf().disable();
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
//                .antMatchers("/ws/*").permitAll()
//                .antMatchers("/api/auth/login").permitAll()
                .and()
                .authorizeRequests()

                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();

    }

    @Bean
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpointImpl projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpointImpl taskEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(
            @NotNull final AuthEndpointImpl authEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

}
