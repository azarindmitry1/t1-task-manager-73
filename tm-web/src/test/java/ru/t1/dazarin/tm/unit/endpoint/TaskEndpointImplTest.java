package ru.t1.dazarin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Category(UnitCategory.class)
public class TaskEndpointImplTest {

    @NotNull
    private final static String TASK_API_URL = "http://localhost:8080/api/task/";

    @NotNull
    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @NotNull
    private TaskDto task = new TaskDto();

    @NotNull
    @Autowired
    private TaskDtoService taskDtoService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        task.setUserId(userId);
        updateMock(task);
    }

    @After
    @SneakyThrows
    public void tearDown() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_API_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @SneakyThrows
    private TaskDto findByIdMock(@NotNull final String id) {
        @NotNull final String findByIdUrl = TASK_API_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(findByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        if (json.isEmpty()) return null;
        return mapper.readValue(json, TaskDto.class);
    }

    @Test
    public void findById() {
        @NotNull final String taskId = task.getId();
        @Nullable final TaskDto taskDto = findByIdMock(taskId);
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskId, taskDto.getId());
    }

    @SneakyThrows
    private List<TaskDto> findAllMock() {
        @NotNull final String existsByIdUrl = TASK_API_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        return Arrays.asList(mapper.readValue(json, TaskDto[].class));
    }

    @Test
    public void findAll() {
        @NotNull final TaskDto taskId = findByIdMock(task.getId());
        @NotNull final List<TaskDto> tasks = findAllMock();
        @NotNull final TaskDto taskDto = tasks.get(0);
        Assert.assertEquals(taskId.getId(), taskDto.getId());
    }

    @SneakyThrows
    private void updateMock(@NotNull final TaskDto taskDto) {
        @NotNull final String url = TASK_API_URL + "update";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void update() {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setName("TEST");
        taskDto.setUserId(userId);
        @NotNull final String taskId = taskDto.getId();
        updateMock(taskDto);
        TaskDto taskById = findByIdMock(taskId);
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskId, taskById.getId());
        Assert.assertEquals("TEST", taskById.getName());
        Assert.assertEquals(userId, taskById.getUserId());
    }

    @SneakyThrows
    private void deleteByIdMock(@NotNull final String id) {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_API_URL + "deleteById/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteById() {
        @NotNull final String taskId = task.getId();
        @Nullable TaskDto taskDto = findByIdMock(taskId);
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskId, taskDto.getId());
        deleteByIdMock(taskId);
        taskDto = findByIdMock(taskId);
        Assert.assertNull(taskDto);
    }

    @SneakyThrows
    private void deleteAllMock(@NotNull final List<TaskDto> tasks) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_API_URL + "deleteAll")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAll() {
        @NotNull final String taskId = task.getId();
        @Nullable TaskDto taskDto = findByIdMock(taskId);
        Assert.assertNotNull(taskDto);
        Assert.assertEquals(taskId, taskDto.getId());
        List<TaskDto> tasks = new ArrayList<>();
        tasks.add(taskDto);
        deleteAllMock(tasks);
        taskDto = findByIdMock(taskId);
        Assert.assertNull(taskDto);
    }

}
