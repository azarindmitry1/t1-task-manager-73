package ru.t1.dazarin.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dazarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dazarin.tm.exception.field.UserIdEmptyException;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@Category(UnitCategory.class)
public class ProjectDtoServiceTest {

    @NotNull
    private static String USER_ID;

    @NotNull
    @Autowired
    private ProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private ProjectDto projectOne;

    @Before
    public void setUp() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectDtoService.deleteAll(USER_ID);
        projectOne = projectDtoService.create(USER_ID);
    }

    @After
    public void tearDown() {
        projectDtoService.deleteAll(USER_ID);
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findAll(null));
        @Nullable final List<ProjectDto> projects = projectDtoService.findAll(USER_ID).stream().collect(Collectors.toList());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findById(null, projectOne.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectDtoService.findById(USER_ID, null));
        @Nullable final ProjectDto projectDto = projectDtoService.findById(USER_ID, projectOne.getId());
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void create() {
        projectDtoService.deleteAll(USER_ID);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.create(null));
        @NotNull final ProjectDto projectDto = projectDtoService.create(USER_ID);
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void save() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.save(null, projectOne));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectDtoService.save(USER_ID, null));
        @Nullable ProjectDto projectDto = projectDtoService.findById(USER_ID, projectOne.getId());
        Assert.assertNotNull(projectDto);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertNotEquals(newStatus, projectDto.getStatus());
        projectDto.setStatus(newStatus);
        projectDtoService.save(USER_ID, projectDto);
        projectDto = projectDtoService.findById(USER_ID, projectOne.getId());
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectDto.getStatus(), newStatus);
    }

    @Test
    public void deleteById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteById(null, projectOne.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectDtoService.deleteById(USER_ID, null));
        @Nullable ProjectDto projectDto = projectDtoService.findById(USER_ID, projectOne.getId());
        Assert.assertNotNull(projectDto);
        projectDtoService.deleteById(USER_ID, projectDto.getId());
        projectDto = projectDtoService.findById(USER_ID, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteById(null, projectOne.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectDtoService.deleteById(USER_ID, null));
        @Nullable ProjectDto projectDto = projectDtoService.findById(USER_ID, projectOne.getId());
        Assert.assertNotNull(projectDto);
        projectDtoService.deleteById(USER_ID, projectOne.getId());
        projectDto = projectDtoService.findById(USER_ID, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void deleteAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteAll(null));
        Assert.assertEquals(1L, projectDtoService.findAll(USER_ID).size());
        projectDtoService.deleteAll(USER_ID);
        Assert.assertEquals(0L, projectDtoService.findAll(USER_ID).size());
    }

}
