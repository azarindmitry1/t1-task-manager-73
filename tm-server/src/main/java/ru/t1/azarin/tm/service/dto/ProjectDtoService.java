package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.dto.IProjectDtoService;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.repository.dto.ProjectDtoRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository repository;

    @Override
    @Transactional
    public void add(@Nullable final ProjectDto model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        return repository.saveAndFlush(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return repository.saveAndFlush(project);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDto project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.saveAndFlush(project);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAllByUserIdWithSort(userId, sort.getColumnName());
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    public void remove(@Nullable final ProjectDto model) {
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsById(model.getId())) throw new ProjectNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (findOneById(userId, id) == null) throw new ProjectNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public ProjectDto update(@Nullable final ProjectDto model) {
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsById(model.getId())) throw new ProjectNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Override
    public ProjectDto updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }

}