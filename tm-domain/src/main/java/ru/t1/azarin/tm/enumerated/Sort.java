package ru.t1.azarin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Sort {

    BY_CREATED("Sort by created", "created"),
    BY_NAME("Sort by name", "name"),
    BY_STATUS("Sort by status", "status");

    @NotNull
    private final String displayName;

    @NotNull
    private final String columnName;

    Sort(@NotNull final String displayName, @NotNull final String columnName) {
        this.displayName = displayName;
        this.columnName = columnName;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}